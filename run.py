import time
from multiprocessing import Pool
import re
import json
from collections import defaultdict
import copy

from tqdm import tqdm
from Bio import Entrez, SeqIO

aa = {}
aa['A'] = 0
aa['R'] = 1
aa['N'] = 0
aa['D'] = -1
aa['B'] = -1
aa['C'] = 0
aa['E'] = -1
aa['Q'] = 0
aa['Z'] = -1
aa['G'] = 0
aa['H'] = 0
aa['I'] = 0
aa['L'] = 0
aa['K'] = 1
aa['M'] = 0
aa['F'] = 0
aa['P'] = 0
aa['S'] = 0
aa['T'] = 0
aa['W'] = 0
aa['Y'] = 0
aa['V'] = 0

handle = open('sequence.gp','ru')
parsed = list(SeqIO.parse(handle,'genbank'))
results = defaultdict(dict)
allchargeDensities = []
for i in tqdm(range(len(parsed))):
	a=parsed[i]
	pid = a.id
	try:
		results[pid]['decription'] = a.description
	except:
		results[pid]['decription'] = ''
	try:
		results[pid]['gi'] = a.annotations['gi']
	except:
		results[pid]['gi'] = ''
	try:
		results[pid]['source'] = a.annotations['source']
	except:
		results[pid]['source'] = ''
	try:
		results[pid]['db_source'] = a.annotations['db_source']
	except:
		results[pid]['db_source'] = ''
	try:
		results[pid]['title'] = a.annotations['references'][0].title 
		results[pid]['authors'] = a.annotations['references'][0].authors
	except:
		results[pid]['title'] = ''
		results[pid]['authors'] = ''
	try:
		results[pid]['seq'] = list(a.seq)
		results[pid]['ca_region'] = []
		for subfeat in a.features:
			if subfeat.type == 'Het' and 'CA,' in str(subfeat):
				results[pid]['ca_region'].append(list(set(list(subfeat.location))))
		results[pid]['chargedensity'] = []
		for region in results[pid]['ca_region']:
			residues = copy.deepcopy(region)
			for eachAA in region:
				residues.append(eachAA-1)
				residues.append(eachAA+1)
			residues = list(set(residues))
			totalCharge = 0
			totalResidues = float(len(residues))
			for residue in residues:
				try:
					totalCharge += aa[results[pid]['seq'][residue]]
				except:
					totalResidues += -1
			if totalResidues > 0:
				chargeDensity = float(totalCharge)/totalResidues
				results[pid]['chargedensity'].append(chargeDensity)
				allchargeDensities.append(chargeDensity)
	except:
		print("Deleting " + pid)
		try:
			del results[pid]
		except:
			pass

# print(json.dumps(results,indent=2))

with open('chargeDensities.json','w') as f:
	f.write(json.dumps(allchargeDensities))

with open('results.json','w') as f:
	f.write(json.dumps(results))
