from pypdb import *
import json
from collections import defaultdict

from deco import *

@concurrent
def getPDB(pid):
	print(pid)
	return get_all_info(pid)

@synchronized
def getAllPDBs(a):
	results = defaultdict(dict)
	keysubset = []
	for key in a.keys():
		keysubset.append(key.split('_')[0].lower())
	keysubset = list(set(keysubset))
	print(len(keysubset))
	for pid in keysubset:
		results[pid] = getPDB(pid)
	return results

r = getAllPDBs(json.load(open('results.json','r')))
with open('resultsPDBs.json','w') as f:
	f.write(json.dumps(r,indent=2))