import json
import copy
from collections import defaultdict
r = json.load(open('results.json','r'))

results = defaultdict(dict)
for key in r.keys():
	newkey = key.split('_')[0].lower()
	if newkey in results:
		if len(r[key]['ca_region']) <= len(results[newkey]['ca_region']):
			continue
	results[newkey] = copy.deepcopy(r[key])

with open('resultsFiltered.json','w') as f:
	f.write(json.dumps(results,indent=2))

a = results
basicUrl = "http://www.ncbi.nlm.nih.gov/pubmed/?term=%s[Title]\n"
urls = []
charges = []
lens = []
highlycharged = []
for pid in a.keys():
	for i,charge in enumerate(a[pid]['chargedensity']):
		aas = a[pid]['ca_region'][i]
		if len(aas) == 1:
			continue
		aas = sorted(aas)
		nearby = True
		for j in range(1,len(aas)):
			if aas[j]-aas[j-1] > 6:
				nearby = False
				break
		if nearby:
			continue
		charges.append(charge)
		lens.append(len(a[pid]['seq']))
		if charge <= -0.4:
			highlycharged.append(pid.split('_')[0])
			urls.append((basicUrl % '+'.join(a[pid]['title'].split())).lower())
			break

with open('data.json','w') as f:
	f.write(json.dumps(charges))
	f.write('\n')
	f.write(json.dumps(lens))

urls = list(set(urls))
with open('urls.txt','w') as f:
	for url in urls:
		f.write(url)

highlycharged = list(set(highlycharged))
with open('highlycharged.json','w') as f:
	f.write(json.dumps(highlycharged))
