LOCUS       1PRR_A                   173 aa            linear   BCT 01-FEB-2013
DEFINITION  Chain A, Nmr-derived Three-dimensional Solution Structure Of
            Protein S Complexed With Calcium.
ACCESSION   1PRR_A
VERSION     1PRR_A  GI:157833560
DBSOURCE    pdb: molecule 1PRR, chain 65, release Jan 25, 2013;
            deposition: Mar 25, 1994;
            class: Binding Protein;
            source: Mmdb_id: 57166, Pdb_id 1: 1PRR;
            Exp. method: Solution Nmr.
KEYWORDS    .
SOURCE      Myxococcus xanthus
  ORGANISM  Myxococcus xanthus
            Bacteria; Proteobacteria; Deltaproteobacteria; Myxococcales;
            Cystobacterineae; Myxococcaceae; Myxococcus.
REFERENCE   1  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     NMR-derived three-dimensional solution structure of protein S
            complexed with calcium
  JOURNAL   Structure 2 (2), 107-122 (1994)
   PUBMED   8081742
REFERENCE   2  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Kay,L.E., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     Unusual helix-containing greek keys in development-specific
            Ca(2+)-binding protein S. 1H, 15N, and 13C assignments and
            secondary structure determined with the use of multidimensional
            double and triple resonance heteronuclear NMR spectroscopy
  JOURNAL   Biochemistry 33 (9), 2409-2421 (1994)
   PUBMED   8117701
REFERENCE   3  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     Structural similarity of a developmentally regulated bacterial
            spore coat protein to beta gamma-crystallins of the vertebrate eye
            lens
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 91 (10), 4308-4312 (1994)
   PUBMED   8183906
REFERENCE   4  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     Direct Submission
  JOURNAL   Submitted (25-MAR-1994)
COMMENT     1 Development-specific Protein S.
FEATURES             Location/Qualifiers
     source          1..173
                     /organism="Myxococcus xanthus"
                     /db_xref="taxon:34"
     Region          1..89
                     /region_name="Domain 1"
                     /note="NCBI Domains"
     SecStr          3..8
                     /sec_str_type="sheet"
                     /note="strand 1"
     Region          4..85
                     /region_name="XTALbg"
                     /note="Beta/gamma crystallins; smart00247"
                     /db_xref="CDD:214583"
     Het             join(bond(10),bond(10),bond(71),bond(10),bond(10),
                     bond(10),bond(71))
                     /heterogen="( CA,1000 )"
     SecStr          15..20
                     /sec_str_type="sheet"
                     /note="strand 2"
     SecStr          21..25
                     /sec_str_type="sheet"
                     /note="strand 3"
     SecStr          26..33
                     /sec_str_type="helix"
                     /note="helix 1"
     SecStr          40..46
                     /sec_str_type="sheet"
                     /note="strand 4"
     SecStr          47..55
                     /sec_str_type="sheet"
                     /note="strand 5"
     Region          49..133
                     /region_name="Crystall"
                     /note="Beta/Gamma crystallin; cl02528"
                     /db_xref="CDD:271588"
     SecStr          60..65
                     /sec_str_type="sheet"
                     /note="strand 6"
     SecStr          66..70
                     /sec_str_type="sheet"
                     /note="strand 7"
     SecStr          80..87
                     /sec_str_type="sheet"
                     /note="strand 8"
     Region          90..173
                     /region_name="Domain 2"
                     /note="NCBI Domains"
     SecStr          91..98
                     /sec_str_type="sheet"
                     /note="strand 9"
     Het             join(bond(99),bond(159),bond(99),bond(99),bond(99),
                     bond(99),bond(159))
                     /heterogen="( CA,1001 )"
     SecStr          103..109
                     /sec_str_type="sheet"
                     /note="strand 10"
     SecStr          110..114
                     /sec_str_type="sheet"
                     /note="strand 11"
     SecStr          115..122
                     /sec_str_type="helix"
                     /note="helix 2"
     SecStr          129..135
                     /sec_str_type="sheet"
                     /note="strand 12"
     SecStr          137..143
                     /sec_str_type="sheet"
                     /note="strand 13"
     Region          138..>172
                     /region_name="Crystall"
                     /note="Beta/Gamma crystallin; cl02528"
                     /db_xref="CDD:271588"
     SecStr          148..154
                     /sec_str_type="sheet"
                     /note="strand 14"
     SecStr          168..173
                     /sec_str_type="sheet"
                     /note="strand 15"
ORIGIN      
        1 manitvfyne dfqgkqvdlp pgnytraqla algienntis svkvppgvka ilyqndgfag
       61 dqievvanae elgplnnnvs sirvisvpvq prarffykeq fdgkevdlpp gqytqaeler
      121 ygidnntiss vkpqglavvl fkndnfsgdt lpvnsdaptl gamnnntssi ris
//

LOCUS       1PRS_A                   173 aa            linear   BCT 10-OCT-2012
DEFINITION  Chain A, Nmr-Derived Three-Dimensional Solution Structure Of
            Protein S Complexed With Calcium.
ACCESSION   1PRS_A
VERSION     1PRS_A  GI:157833561
DBSOURCE    pdb: molecule 1PRS, chain 65, release Sep 12, 2012;
            deposition: Mar 25, 1994;
            class: Binding Protein;
            source: Mmdb_id: 57167, Pdb_id 1: 1PRS;
            Exp. method: Solution Nmr.
KEYWORDS    .
SOURCE      Myxococcus xanthus
  ORGANISM  Myxococcus xanthus
            Bacteria; Proteobacteria; Deltaproteobacteria; Myxococcales;
            Cystobacterineae; Myxococcaceae; Myxococcus.
REFERENCE   1  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     NMR-derived three-dimensional solution structure of protein S
            complexed with calcium
  JOURNAL   Structure 2 (2), 107-122 (1994)
   PUBMED   8081742
REFERENCE   2  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Kay,L.E., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     Unusual helix-containing greek keys in development-specific
            Ca(2+)-binding protein S. 1H, 15N, and 13C assignments and
            secondary structure determined with the use of multidimensional
            double and triple resonance heteronuclear NMR spectroscopy
  JOURNAL   Biochemistry 33 (9), 2409-2421 (1994)
   PUBMED   8117701
REFERENCE   3  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     Structural similarity of a developmentally regulated bacterial
            spore coat protein to beta gamma-crystallins of the vertebrate eye
            lens
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 91 (10), 4308-4312 (1994)
   PUBMED   8183906
REFERENCE   4  (residues 1 to 173)
  AUTHORS   Bagby,S., Harvey,T.S., Eagle,S.G., Inouye,S. and Ikura,M.
  TITLE     Direct Submission
  JOURNAL   Submitted (25-MAR-1994)
COMMENT     1 Development-specific Protein S.
FEATURES             Location/Qualifiers
     source          1..173
                     /organism="Myxococcus xanthus"
                     /db_xref="taxon:34"
     Region          1..89
                     /region_name="Domain 1"
                     /note="NCBI Domains"
     SecStr          3..7
                     /sec_str_type="sheet"
                     /note="strand 1"
     Region          4..85
                     /region_name="XTALbg"
                     /note="Beta/gamma crystallins; smart00247"
                     /db_xref="CDD:214583"
     SecStr          8..11
                     /sec_str_type="sheet"
                     /note="strand 2"
     Het             join(bond(10),bond(71),bond(71),bond(10),bond(10),
                     bond(10))
                     /heterogen="( CA,1000 )"
     SecStr          12..15
                     /sec_str_type="sheet"
                     /note="strand 3"
     SecStr          16..20
                     /sec_str_type="sheet"
                     /note="strand 4"
     SecStr          21..25
                     /sec_str_type="sheet"
                     /note="strand 5"
     SecStr          26..33
                     /sec_str_type="helix"
                     /note="helix 1"
     SecStr          40..44
                     /sec_str_type="sheet"
                     /note="strand 6"
     SecStr          47..55
                     /sec_str_type="sheet"
                     /note="strand 7"
     Region          49..133
                     /region_name="Crystall"
                     /note="Beta/Gamma crystallin; cl02528"
                     /db_xref="CDD:271588"
     SecStr          60..66
                     /sec_str_type="sheet"
                     /note="strand 8"
     SecStr          67..70
                     /sec_str_type="sheet"
                     /note="strand 9"
     SecStr          80..87
                     /sec_str_type="sheet"
                     /note="strand 10"
     Region          90..173
                     /region_name="Domain 2"
                     /note="NCBI Domains"
     SecStr          91..98
                     /sec_str_type="sheet"
                     /note="strand 11"
     Het             join(bond(99),bond(130),bond(130),bond(159),bond(160),
                     bond(161),bond(99),bond(99),bond(99),bond(130),bond(159),
                     bond(159),bond(159),bond(159),bond(159),bond(160),
                     bond(160),bond(161))
                     /heterogen="( CA,1001 )"
     SecStr          103..109
                     /sec_str_type="sheet"
                     /note="strand 12"
     SecStr          110..113
                     /sec_str_type="sheet"
                     /note="strand 13"
     SecStr          114..121
                     /sec_str_type="helix"
                     /note="helix 2"
     SecStr          129..135
                     /sec_str_type="sheet"
                     /note="strand 14"
     SecStr          137..143
                     /sec_str_type="sheet"
                     /note="strand 15"
     Region          138..>172
                     /region_name="Crystall"
                     /note="Beta/Gamma crystallin; cl02528"
                     /db_xref="CDD:271588"
     SecStr          148..154
                     /sec_str_type="sheet"
                     /note="strand 16"
     SecStr          168..173
                     /sec_str_type="sheet"
                     /note="strand 17"
ORIGIN      
        1 manitvfyne dfqgkqvdlp pgnytraqla algienntis svkvppgvka ilyqndgfag
       61 dqievvanae elgplnnnvs sirvisvpvq prarffykeq fdgkevdlpp gqytqaeler
      121 ygidnntiss vkpqglavvl fkndnfsgdt lpvnsdaptl gamnnntssi ris
//
