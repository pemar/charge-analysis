LOCUS       4L1I_A                   230 aa            linear   INV 08-JAN-2014
DEFINITION  Chain A, Crystal Structure Of Egfp-based Calcium Sensor Catcher
            Complexed With Ca.
ACCESSION   4L1I_A
VERSION     4L1I_A  GI:564731095
DBSOURCE    pdb: molecule 4L1I, chain 65, release Jan 8, 2014;
            deposition: Jun 3, 2013;
            class: Calcium Binding Protein;
            source: Mmdb_id: 115859, Pdb_id 1: 4L1I;
            Exp. method: X-Ray Diffraction.
KEYWORDS    .
SOURCE      Aequorea victoria
  ORGANISM  Aequorea victoria
            Eukaryota; Metazoa; Cnidaria; Hydrozoa; Hydroidolina; Leptothecata;
            Aequoreidae; Aequorea.
REFERENCE   1  (residues 1 to 230)
  AUTHORS   Zhang,Y., Reddish,F., Tang,S., Zhuo,Y., Wang,Y.F., Yang,J.J. and
            Weber,I.T.
  TITLE     Structural basis for a hand-like site in the calcium sensor CatchER
            with fast kinetics
  JOURNAL   Acta Crystallogr. D Biol. Crystallogr. 69 (PT 12), 2309-2319 (2013)
   PUBMED   24311573
REFERENCE   2  (residues 1 to 230)
  AUTHORS   Zhang,Y. and Weber,I.T.
  TITLE     Direct Submission
  JOURNAL   Submitted (03-JUN-2013)
COMMENT     1 Egfp-based Calcium Sensor Catcher.
FEATURES             Location/Qualifiers
     source          1..230
                     /organism="Aequorea victoria"
                     /db_xref="taxon:6100"
     Region          7..228
                     /region_name="GFP"
                     /note="Green fluorescent protein; pfam01353"
                     /db_xref="CDD:250555"
     SecStr          12..20
                     /sec_str_type="sheet"
                     /note="strand 1"
     SecStr          21..24
                     /sec_str_type="sheet"
                     /note="strand 2"
     SecStr          26..33
                     /sec_str_type="sheet"
                     /note="strand 3"
     SecStr          34..39
                     /sec_str_type="sheet"
                     /note="strand 4"
     SecStr          40..51
                     /sec_str_type="sheet"
                     /note="strand 5"
     SecStr          89..100
                     /sec_str_type="sheet"
                     /note="strand 6"
     SecStr          102..115
                     /sec_str_type="sheet"
                     /note="strand 7"
     SecStr          116..128
                     /sec_str_type="sheet"
                     /note="strand 8"
     Het             join(146,201,201,146,201)
                     /heterogen="( CA,1000 )"
     Het             join(146,201,146,201,201)
                     /heterogen="( CA,1001 )"
     SecStr          146..155
                     /sec_str_type="sheet"
                     /note="strand 9"
     SecStr          158..171
                     /sec_str_type="sheet"
                     /note="strand 10"
     SecStr          173..181
                     /sec_str_type="sheet"
                     /note="strand 11"
     SecStr          182..188
                     /sec_str_type="sheet"
                     /note="strand 12"
     SecStr          197..208
                     /sec_str_type="sheet"
                     /note="strand 13"
     SecStr          215..227
                     /sec_str_type="sheet"
                     /note="strand 14"
ORIGIN      
        1 mvskgeelft gvvpilveld gdvnghkfsv sgegegdaty gkltlkfict tgklpvpwpt
       61 lvttlxvqcf srypdhmkqh dffksampeg yvqertiffk ddgnyktrae vkfegdtlvn
      121 rielkgidfk edgnilghkl eynynehnvy itadkqkngi kanfkirhni edgsvqladh
      181 yqqntpigdg pvllpdnhyl dtesalskdp nekrdhmvll eeveaagitl
//

LOCUS       2ZBD_A                   995 aa            linear   MAM 10-JAN-2013
DEFINITION  Chain A, Crystal Structure Of The Sr Calcium Pump With Bound
            Aluminium Fluoride, Adp And Calcium.
ACCESSION   2ZBD_A
VERSION     2ZBD_A  GI:440923701
DBSOURCE    pdb: molecule 2ZBD, chain 65, release Jan 4, 2013;
            deposition: Oct 20, 2007;
            class: Hydrolase;
            source: Mmdb_id: 106354, Pdb_id 1: 2ZBD;
            Exp. method: X-Ray Diffraction.
KEYWORDS    .
SOURCE      Oryctolagus cuniculus (rabbit)
  ORGANISM  Oryctolagus cuniculus
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Mammalia; Eutheria; Euarchontoglires; Glires; Lagomorpha;
            Leporidae; Oryctolagus.
REFERENCE   1  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nakasako,M., Nomura,H. and Ogawa,H.
  TITLE     Crystal structure of the calcium pump of sarcoplasmic reticulum at
            2.6 A resolution
  JOURNAL   Nature 405 (6787), 647-655 (2000)
   PUBMED   10864315
REFERENCE   2  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Nomura,H.
  TITLE     Structural changes in the calcium pump accompanying the
            dissociation of calcium
  JOURNAL   Nature 418 (6898), 605-611 (2002)
   PUBMED   12167852
REFERENCE   3  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Mizutani,T.
  TITLE     Crystal structure of the calcium pump with a bound ATP analogue
  JOURNAL   Nature 430 (6999), 529-535 (2004)
   PUBMED   15229613
REFERENCE   4  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nomura,H. and Tsuda,T.
  TITLE     Lumenal gating mechanism revealed in calcium pump crystal
            structures with phosphate analogues
  JOURNAL   Nature 432 (7015), 361-368 (2004)
   PUBMED   15448704
REFERENCE   5  (residues 1 to 995)
  AUTHORS   Obara,K., Miyashita,N., Xu,C., Toyoshima,I., Sugita,Y., Inesi,G.
            and Toyoshima,C.
  TITLE     Structural role of countertransport revealed in Ca(2+) pump crystal
            structure in the absence of Ca(2+)
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 102 (41), 14489-14496 (2005)
   PUBMED   16150713
REFERENCE   6  (residues 1 to 995)
  AUTHORS   Takahashi,M., Kondou,Y. and Toyoshima,C.
  TITLE     Interdomain communication in calcium pump as revealed in the
            crystal structures with transmembrane inhibitors
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 104 (14), 5800-5805 (2007)
   PUBMED   17389383
REFERENCE   7  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nomura,H., Tsuda,T., Ogawa,H. and Norimatsu,Y.
  TITLE     Direct Submission
  JOURNAL   Submitted (20-OCT-2007)
COMMENT     On Jan 10, 2013 this sequence version replaced gi:160877883.
            1 Sarcoplasmic/endoplasmic Reticulum Calcium Atpase 1.
FEATURES             Location/Qualifiers
     source          1..995
                     /organism="Oryctolagus cuniculus"
                     /db_xref="taxon:9986"
     Region          5..73
                     /region_name="Cation_ATPase_N"
                     /note="Cation transporter/ATPase, N-terminus; pfam00690"
                     /db_xref="CDD:250059"
     SecStr          10..17
                     /sec_str_type="helix"
                     /note="helix 1"
     SecStr          27..36
                     /sec_str_type="helix"
                     /note="helix 2"
     Region          join(44..124,238..346,741..995)
                     /region_name="Domain 1"
                     /note="NCBI Domains"
     SecStr          51..58
                     /sec_str_type="helix"
                     /note="helix 3"
     Region          54..990
                     /region_name="ATPase-IIA1_Ca"
                     /note="sarco/endoplasmic reticulum calcium-translocating
                     P-type ATPase; TIGR01116"
                     /db_xref="CDD:273452"
     SecStr          61..77
                     /sec_str_type="helix"
                     /note="helix 4"
     SecStr          91..122
                     /sec_str_type="helix"
                     /note="helix 5"
     Region          94..342
                     /region_name="E1-E2_ATPase"
                     /note="E1-E2 ATPase; pfam00122"
                     /db_xref="CDD:249606"
     Region          125..237
                     /region_name="Domain 2"
                     /note="NCBI Domains"
     SecStr          127..131
                     /sec_str_type="sheet"
                     /note="strand 1"
     SecStr          138..142
                     /sec_str_type="sheet"
                     /note="strand 2"
     SecStr          150..156
                     /sec_str_type="sheet"
                     /note="strand 3"
     SecStr          161..165
                     /sec_str_type="sheet"
                     /note="strand 4"
     SecStr          166..171
                     /sec_str_type="sheet"
                     /note="strand 5"
     SecStr          174..178
                     /sec_str_type="sheet"
                     /note="strand 6"
     SecStr          187..190
                     /sec_str_type="sheet"
                     /note="strand 7"
     SecStr          207..212
                     /sec_str_type="sheet"
                     /note="strand 8"
     SecStr          213..217
                     /sec_str_type="sheet"
                     /note="strand 9"
     SecStr          218..225
                     /sec_str_type="sheet"
                     /note="strand 10"
     SecStr          249..276
                     /sec_str_type="helix"
                     /note="helix 6"
     SecStr          289..307
                     /sec_str_type="helix"
                     /note="helix 7"
     Het             join(305,306,308,310,310,797,801,306,310)
                     /heterogen="( CA,1001 )"
     SecStr          312..330
                     /sec_str_type="helix"
                     /note="helix 8"
     SecStr          331..335
                     /sec_str_type="sheet"
                     /note="strand 11"
     SecStr          338..346
                     /sec_str_type="helix"
                     /note="helix 9"
     Region          join(347..357,606..740)
                     /region_name="Domain 3"
                     /note="NCBI Domains"
     SecStr          347..353
                     /sec_str_type="sheet"
                     /note="strand 12"
     Het             join(352,354,704,704)
                     /heterogen="( MG,1004 )"
     Het             352
                     /heterogen="(ALF,1003 )"
     Region          358..605
                     /region_name="Domain 4"
                     /note="NCBI Domains"
     SecStr          362..370
                     /sec_str_type="sheet"
                     /note="strand 13"
     SecStr          371..375
                     /sec_str_type="sheet"
                     /note="strand 14"
     SecStr          376..387
                     /sec_str_type="sheet"
                     /note="strand 15"
     SecStr          393..398
                     /sec_str_type="sheet"
                     /note="strand 16"
     SecStr          409..421
                     /sec_str_type="helix"
                     /note="helix 10"
     Region          419..529
                     /region_name="Hydrolase_like2"
                     /note="Putative hydrolase of sodium-potassium ATPase alpha
                     subunit; pfam13246"
                     /db_xref="CDD:257602"
     SecStr          424..431
                     /sec_str_type="sheet"
                     /note="strand 17"
     SecStr          432..439
                     /sec_str_type="sheet"
                     /note="strand 18"
     SecStr          441..452
                     /sec_str_type="helix"
                     /note="helix 11"
     SecStr          471..478
                     /sec_str_type="helix"
                     /note="helix 12"
     SecStr          479..484
                     /sec_str_type="sheet"
                     /note="strand 19"
     SecStr          485..490
                     /sec_str_type="sheet"
                     /note="strand 20"
     SecStr          492..502
                     /sec_str_type="sheet"
                     /note="strand 21"
     SecStr          511..518
                     /sec_str_type="sheet"
                     /note="strand 22"
     SecStr          519..526
                     /sec_str_type="helix"
                     /note="helix 13"
     SecStr          527..532
                     /sec_str_type="sheet"
                     /note="strand 23"
     SecStr          533..538
                     /sec_str_type="sheet"
                     /note="strand 24"
     SecStr          540..555
                     /sec_str_type="helix"
                     /note="helix 14"
     SecStr          561..569
                     /sec_str_type="sheet"
                     /note="strand 25"
     SecStr          592..597
                     /sec_str_type="sheet"
                     /note="strand 26"
     SecStr          598..602
                     /sec_str_type="sheet"
                     /note="strand 27"
     SecStr          608..618
                     /sec_str_type="helix"
                     /note="helix 15"
     Region          <612..721
                     /region_name="HAD_like"
                     /note="Haloacid dehalogenase-like hydrolases. The haloacid
                     dehalogenase-like (HAD) superfamily includes L-2-haloacid
                     dehalogenase, epoxide hydrolase, phosphoserine
                     phosphatase, phosphomannomutase, phosphoglycolate
                     phosphatase, P-type ATPase, and many others; cd01427"
                     /db_xref="CDD:119389"
     SecStr          620..627
                     /sec_str_type="sheet"
                     /note="strand 28"
     Site            626
                     /site_type="other"
                     /note="motif II"
                     /db_xref="CDD:119389"
     SecStr          630..640
                     /sec_str_type="helix"
                     /note="helix 16"
     SecStr          652..656
                     /sec_str_type="sheet"
                     /note="strand 29"
     SecStr          664..673
                     /sec_str_type="helix"
                     /note="helix 17"
     SecStr          675..679
                     /sec_str_type="sheet"
                     /note="strand 30"
     SecStr          682..695
                     /sec_str_type="helix"
                     /note="helix 18"
     SecStr          698..704
                     /sec_str_type="sheet"
                     /note="strand 31"
     SecStr          708..715
                     /sec_str_type="helix"
                     /note="helix 19"
     SecStr          716..722
                     /sec_str_type="sheet"
                     /note="strand 32"
     SecStr          725..732
                     /sec_str_type="helix"
                     /note="helix 20"
     SecStr          733..737
                     /sec_str_type="sheet"
                     /note="strand 33"
     SecStr          743..782
                     /sec_str_type="helix"
                     /note="helix 21"
     Het             join(769,772,800..801,909)
                     /heterogen="( CA,1000 )"
     Region          785..988
                     /region_name="Cation_ATPase_C"
                     /note="Cation transporting ATPase, C-terminus; pfam00689"
                     /db_xref="CDD:250058"
     SecStr          790..801
                     /sec_str_type="helix"
                     /note="helix 22"
     SecStr          832..857
                     /sec_str_type="helix"
                     /note="helix 23"
     Bond            877..889
                     /bond_type="disulfide"
     SecStr          896..915
                     /sec_str_type="helix"
                     /note="helix 24"
     SecStr          932..950
                     /sec_str_type="helix"
                     /note="helix 25"
     SecStr          965..975
                     /sec_str_type="helix"
                     /note="helix 26"
     SecStr          978..990
                     /sec_str_type="helix"
                     /note="helix 27"
ORIGIN      
        1 xmeaahskst eeclayfgvs ettgltpdqv krhlekyghn elpaeegksl welvieqfed
       61 llvrilllaa cisfvlawfe egeetitafv epfvillili anaivgvwqe rnaenaieal
      121 keyepemgkv yradrksvqr ikardivpgd ivevavgdkv padirilsik sttlrvdqsi
      181 ltgesvsvik htepvpdpra vnqdkknmlf sgtniaagka lgivattgvs teigkirdqm
      241 aateqdktpl qqkldefgeq lskvislicv avwlinighf ndpvhggswi rgaiyyfkia
      301 valavaaipe glpavittcl algtrrmakk naivrslpsv etlgctsvic sdktgtlttn
      361 qmsvckmfii dkvdgdfcsl nefsitgsty apegevlknd kpirsgqfdg lvelatical
      421 cndssldfne tkgvyekvge atetalttlv ekmnvfntev rnlskveran acnsvirqlm
      481 kkeftlefsr drksmsvycs pakssraavg nkmfvkgape gvidrcnyvr vgttrvpmtg
      541 pvkekilsvi kewgtgrdtl rclalatrdt ppkreemvld dssrfmeyet dltfvgvvgm
      601 ldpprkevmg siqlcrdagi rvimitgdnk gtaiaicrri gifgeneeva draytgrefd
      661 dlplaeqrea crraccfarv epshkskive ylqsydeita mtgdgvndap alkkaeigia
      721 mgsgtavakt asemvladdn fstivaavee graiynnmkq firylissnv gevvciflta
      781 alglpealip vqllwvnlvt dglpatalgf nppdldimdr pprspkepli sgwlffryma
      841 iggyvgaatv gaaawwfmya edgpgvtyhq lthfmqcted hphfegldce ifeapepmtm
      901 alsvlvtiem cnalnslsen qslmrmppwv niwllgsicl smslhflily vdplpmifkl
      961 kaldltqwlm vlkislpvig ldeilkfiar nyleg
//

LOCUS       5CPV_A                   109 aa            linear   VRT 10-OCT-2012
DEFINITION  Chain A, Restrained Least Squares Refinement Of Native (Calcium)
            And Cadmium-Substituted Carp Parvalbumin Using X-Ray
            Crystallographic Data At 1.6-Angstroms Resolution.
ACCESSION   5CPV_A
VERSION     5CPV_A  GI:313507317
DBSOURCE    pdb: molecule 5CPV, chain 65, release Nov 30, 2010;
            deposition: Jan 24, 1990;
            class: Calcium Binding;
            source: Mmdb_id: 87073, Pdb_id 1: 5CPV;
            Exp. method: X-Ray Diffraction.
KEYWORDS    .
SOURCE      Cyprinus carpio (common carp)
  ORGANISM  Cyprinus carpio
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Actinopterygii; Neopterygii; Teleostei; Ostariophysi;
            Cypriniformes; Cyprinidae; Cyprinus.
REFERENCE   1  (residues 1 to 109)
  AUTHORS   Kretsinger,R.H.
  TITLE     Gene Triplication Deduced From The Tertiary Structure Of A Muscle
            Calcium Binding Protein
  JOURNAL   Nature New Biol. 240, 85 (1972)
REFERENCE   2  (residues 1 to 109)
  AUTHORS   Kretsinger,R.H., Nockolds,C.E., Coffee,C.J. and Bradshaw,R.A.
  TITLE     The structure of a calcium-binding protein from carp muscle
  JOURNAL   Cold Spring Harb. Symp. Quant. Biol. 36, 217-220 (1972)
   PUBMED   4508136
REFERENCE   3  (residues 1 to 109)
  AUTHORS   Mclachlan,A.D.
  TITLE     Gene Duplication In Carp Muscle Calcium Binding Protein
  JOURNAL   Nature New Biol. 240, 83 (1972)
REFERENCE   4  (residues 1 to 109)
  AUTHORS   Nockolds,C.E., Kretsinger,R.H., Coffee,C.J. and Bradshaw,R.A.
  TITLE     Structure of a calcium-binding carp myogen
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 69 (3), 581-584 (1972)
   PUBMED   4501574
REFERENCE   5  (residues 1 to 109)
  AUTHORS   Coffee,C.J. and Bradshaw,R.A.
  TITLE     Carp muscle calcium-binding protein. I. Characterization of the
            tryptic peptides and the complete amino acid sequence of component
            B
  JOURNAL   J. Biol. Chem. 248 (9), 3305-3312 (1973)
   PUBMED   4700462
REFERENCE   6  (residues 1 to 109)
  AUTHORS   Kretsinger,R.H. and Nockolds,C.E.
  TITLE     Carp muscle calcium-binding protein. II. Structure determination
            and general description
  JOURNAL   J. Biol. Chem. 248 (9), 3313-3326 (1973)
   PUBMED   4700463
REFERENCE   7  (residues 1 to 109)
  AUTHORS   Hendrickson,W.A. and Karle,J.
  TITLE     Carp muscle calcium-binding protein. 3. Phase refinement using the
            tangent formula
  JOURNAL   J. Biol. Chem. 248 (9), 3327-3334 (1973)
   PUBMED   4735582
REFERENCE   8  (residues 1 to 109)
  AUTHORS   Coffee,C.J., Bradshaw,R.A. and Kretsinger,R.H.
  TITLE     The coordination of calcium ions by carp muscle calcium binding
            proteins A, B and C
  JOURNAL   Adv. Exp. Med. Biol. 48, 211-233 (1974)
   PUBMED   4611157
REFERENCE   9  (residues 1 to 109)
  AUTHORS   Moews,P.C. and Kretsinger,R.H.
  TITLE     Refinement of the structure of carp muscle calcium-binding
            parvalbumin by model building and difference Fourier analysis
  JOURNAL   J. Mol. Biol. 91 (2), 201-225 (1975)
   PUBMED   1237625
REFERENCE   10 (residues 1 to 109)
  AUTHORS   Moews,P.C. and Kretsinger,R.H.
  TITLE     Terbium replacement of calcium in carp muscle calcium-binding
            parvalbumin: an x-ray crystallographic study
  JOURNAL   J. Mol. Biol. 91 (2), 229-232 (1975)
   PUBMED   1237627
REFERENCE   11 (residues 1 to 109)
  AUTHORS   Tufty,R.M. and Kretsinger,R.H.
  TITLE     Troponin and parvalbumin calcium binding regions predicted in
            myosin light chain and T4 lysozyme
  JOURNAL   Science 187 (4172), 167-169 (1975)
   PUBMED   1111094
REFERENCE   12 (residues 1 to 109)
  AUTHORS   Swain,A.L., Kretsinger,R.H. and Amma,E.L.
  TITLE     Restrained least squares refinement of native (calcium) and
            cadmium-substituted carp parvalbumin using X-ray crystallographic
            data at 1.6-A resolution
  JOURNAL   J. Biol. Chem. 264 (28), 16620-16628 (1989)
   PUBMED   2777802
REFERENCE   13 (residues 1 to 109)
  AUTHORS   Swain,A.L., Kretsinger,R.H. and Amma,E.L.
  TITLE     Direct Submission
  JOURNAL   Submitted (24-JAN-1990)
COMMENT     On Dec 3, 2010 this sequence version replaced gi:157837026.
            SEQRES.
FEATURES             Location/Qualifiers
     source          1..109
                     /organism="Cyprinus carpio"
                     /db_xref="taxon:7962"
     SecStr          9..19
                     /sec_str_type="helix"
                     /note="helix 1"
     SecStr          27..34
                     /sec_str_type="helix"
                     /note="helix 2"
     Region          38..109
                     /region_name="Domain 1"
                     /note="NCBI Domains"
     SecStr          41..51
                     /sec_str_type="helix"
                     /note="helix 3"
     Region          43..108
                     /region_name="EFh"
                     /note="EF-hand, calcium binding motif; A diverse
                     superfamily of calcium sensors and calcium signal
                     modulators; most examples in this alignment model have 2
                     active canonical EF hands. Ca2+ binding induces a
                     conformational change in the EF-hand motif, leading to...;
                     cd00051"
                     /db_xref="CDD:238008"
     Region          49..107
                     /region_name="EF-hand_7"
                     /note="EF-hand domain pair; pfam13499"
                     /db_xref="CDD:257819"
     Site            order(52,54,56,63,91,93,95,102)
                     /site_type="other"
                     /note="Ca2+ binding site [ion binding]"
                     /db_xref="CDD:238008"
     Het             join(52,54,56,58,60,63,63,60,63)
                     /heterogen="( CA,1001 )"
     SecStr          60..67
                     /sec_str_type="helix"
                     /note="helix 4"
     SecStr          80..90
                     /sec_str_type="helix"
                     /note="helix 5"
     Het             join(91,93,95,97,102,102,102)
                     /heterogen="( CA,1000 )"
     SecStr          100..108
                     /sec_str_type="helix"
                     /note="helix 6"
ORIGIN      
        1 xafagvlnda diaaaleack aadsfnhkaf fakvgltsks addvkkafai idqdksgfie
       61 edelklflqn fkadaraltd getktflkag dsdgdgkigv deftalvka
//

LOCUS       2ZBE_B                   995 aa            linear   MAM 27-DEC-2012
DEFINITION  Chain B, Calcium Pump Crystal Structure With Bound Bef3 In The
            Absence Of Calcium And Tg.
ACCESSION   2ZBE_B
VERSION     2ZBE_B  GI:433552067
DBSOURCE    pdb: molecule 2ZBE, chain 66, release Dec 7, 2012;
            deposition: Oct 20, 2007;
            class: Hydrolase;
            source: Mmdb_id: 106112, Pdb_id 1: 2ZBE;
            Exp. method: X-Ray Diffraction.
KEYWORDS    .
SOURCE      Oryctolagus cuniculus (rabbit)
  ORGANISM  Oryctolagus cuniculus
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Mammalia; Eutheria; Euarchontoglires; Glires; Lagomorpha;
            Leporidae; Oryctolagus.
REFERENCE   1  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nakasako,M., Nomura,H. and Ogawa,H.
  TITLE     Crystal structure of the calcium pump of sarcoplasmic reticulum at
            2.6 A resolution
  JOURNAL   Nature 405 (6787), 647-655 (2000)
   PUBMED   10864315
REFERENCE   2  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Nomura,H.
  TITLE     Structural changes in the calcium pump accompanying the
            dissociation of calcium
  JOURNAL   Nature 418 (6898), 605-611 (2002)
   PUBMED   12167852
REFERENCE   3  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Mizutani,T.
  TITLE     Crystal structure of the calcium pump with a bound ATP analogue
  JOURNAL   Nature 430 (6999), 529-535 (2004)
   PUBMED   15229613
REFERENCE   4  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nomura,H. and Tsuda,T.
  TITLE     Lumenal gating mechanism revealed in calcium pump crystal
            structures with phosphate analogues
  JOURNAL   Nature 432 (7015), 361-368 (2004)
   PUBMED   15448704
REFERENCE   5  (residues 1 to 995)
  AUTHORS   Obara,K., Miyashita,N., Xu,C., Toyoshima,I., Sugita,Y., Inesi,G.
            and Toyoshima,C.
  TITLE     Structural role of countertransport revealed in Ca(2+) pump crystal
            structure in the absence of Ca(2+)
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 102 (41), 14489-14496 (2005)
   PUBMED   16150713
REFERENCE   6  (residues 1 to 995)
  AUTHORS   Takahashi,M., Kondou,Y. and Toyoshima,C.
  TITLE     Interdomain communication in calcium pump as revealed in the
            crystal structures with transmembrane inhibitors
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 104 (14), 5800-5805 (2007)
   PUBMED   17389383
REFERENCE   7  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Norimatsu,Y., Iwasawa,S., Tsuda,T. and Ogawa,H.
  TITLE     How processing of aspartylphosphate is coupled to lumenal gating of
            the ion pathway in the calcium pump
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 104 (50), 19831-19836 (2007)
   PUBMED   18077416
REFERENCE   8  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Ogawa,H. and Norimatsu,Y.
  TITLE     Direct Submission
  JOURNAL   Submitted (20-OCT-2007)
COMMENT     On Dec 27, 2012 this sequence version replaced gi:162330211.
            1 Sarcoplasmic/endoplasmic Reticulum Calcium Atpase 1.
FEATURES             Location/Qualifiers
     source          1..995
                     /organism="Oryctolagus cuniculus"
                     /db_xref="taxon:9986"
     Region          5..73
                     /region_name="Cation_ATPase_N"
                     /note="Cation transporter/ATPase, N-terminus; pfam00690"
                     /db_xref="CDD:250059"
     SecStr          10..17
                     /sec_str_type="helix"
                     /note="helix 30"
     SecStr          27..36
                     /sec_str_type="helix"
                     /note="helix 31"
     Region          join(44..121,246..309)
                     /region_name="Domain 6"
                     /note="NCBI Domains"
     SecStr          50..58
                     /sec_str_type="helix"
                     /note="helix 32"
     Region          54..990
                     /region_name="ATPase-IIA1_Ca"
                     /note="sarco/endoplasmic reticulum calcium-translocating
                     P-type ATPase; TIGR01116"
                     /db_xref="CDD:273452"
     SecStr          61..79
                     /sec_str_type="helix"
                     /note="helix 33"
     SecStr          91..115
                     /sec_str_type="helix"
                     /note="helix 34"
     Region          94..342
                     /region_name="E1-E2_ATPase"
                     /note="E1-E2 ATPase; pfam00122"
                     /db_xref="CDD:249606"
     Region          122..245
                     /region_name="Domain 7"
                     /note="NCBI Domains"
     SecStr          127..131
                     /sec_str_type="sheet"
                     /note="strand 33"
     SecStr          138..142
                     /sec_str_type="sheet"
                     /note="strand 34"
     SecStr          150..156
                     /sec_str_type="sheet"
                     /note="strand 35"
     SecStr          162..166
                     /sec_str_type="sheet"
                     /note="strand 36"
     SecStr          167..171
                     /sec_str_type="sheet"
                     /note="strand 37"
     SecStr          174..178
                     /sec_str_type="sheet"
                     /note="strand 38"
     SecStr          187..190
                     /sec_str_type="sheet"
                     /note="strand 39"
     SecStr          207..211
                     /sec_str_type="sheet"
                     /note="strand 40"
     SecStr          213..217
                     /sec_str_type="sheet"
                     /note="strand 41"
     SecStr          218..225
                     /sec_str_type="sheet"
                     /note="strand 42"
     SecStr          232..242
                     /sec_str_type="helix"
                     /note="helix 35"
     SecStr          249..272
                     /sec_str_type="helix"
                     /note="helix 36"
     SecStr          299..307
                     /sec_str_type="helix"
                     /note="helix 37"
     Region          join(310..361,606..767)
                     /region_name="Domain 8"
                     /note="NCBI Domains"
     SecStr          312..330
                     /sec_str_type="helix"
                     /note="helix 38"
     SecStr          331..335
                     /sec_str_type="sheet"
                     /note="strand 43"
     SecStr          347..353
                     /sec_str_type="sheet"
                     /note="strand 44"
     Het             join(352,354,704,704)
                     /heterogen="( MG,1001 )"
     Het             352
                     /heterogen="(BEF,1003 )"
     SecStr          357..360
                     /sec_str_type="sheet"
                     /note="strand 45"
     Region          362..605
                     /region_name="Domain 9"
                     /note="NCBI Domains"
     SecStr          362..370
                     /sec_str_type="sheet"
                     /note="strand 46"
     SecStr          371..375
                     /sec_str_type="sheet"
                     /note="strand 47"
     SecStr          376..387
                     /sec_str_type="sheet"
                     /note="strand 48"
     SecStr          394..398
                     /sec_str_type="sheet"
                     /note="strand 49"
     SecStr          409..421
                     /sec_str_type="helix"
                     /note="helix 39"
     Region          419..529
                     /region_name="Hydrolase_like2"
                     /note="Putative hydrolase of sodium-potassium ATPase alpha
                     subunit; pfam13246"
                     /db_xref="CDD:257602"
     SecStr          424..430
                     /sec_str_type="sheet"
                     /note="strand 50"
     SecStr          433..439
                     /sec_str_type="sheet"
                     /note="strand 51"
     SecStr          441..453
                     /sec_str_type="helix"
                     /note="helix 40"
     SecStr          470..478
                     /sec_str_type="helix"
                     /note="helix 41"
     SecStr          480..490
                     /sec_str_type="sheet"
                     /note="strand 52"
     SecStr          492..501
                     /sec_str_type="sheet"
                     /note="strand 53"
     SecStr          510..518
                     /sec_str_type="sheet"
                     /note="strand 54"
     SecStr          519..526
                     /sec_str_type="helix"
                     /note="helix 42"
     SecStr          527..532
                     /sec_str_type="sheet"
                     /note="strand 55"
     SecStr          533..538
                     /sec_str_type="sheet"
                     /note="strand 56"
     SecStr          540..554
                     /sec_str_type="helix"
                     /note="helix 43"
     SecStr          561..569
                     /sec_str_type="sheet"
                     /note="strand 57"
     SecStr          592..597
                     /sec_str_type="sheet"
                     /note="strand 58"
     SecStr          598..602
                     /sec_str_type="sheet"
                     /note="strand 59"
     SecStr          608..618
                     /sec_str_type="helix"
                     /note="helix 44"
     Region          <612..721
                     /region_name="HAD_like"
                     /note="Haloacid dehalogenase-like hydrolases. The haloacid
                     dehalogenase-like (HAD) superfamily includes L-2-haloacid
                     dehalogenase, epoxide hydrolase, phosphoserine
                     phosphatase, phosphomannomutase, phosphoglycolate
                     phosphatase, P-type ATPase, and many others; cd01427"
                     /db_xref="CDD:119389"
     SecStr          620..627
                     /sec_str_type="sheet"
                     /note="strand 60"
     Site            626
                     /site_type="other"
                     /note="motif II"
                     /db_xref="CDD:119389"
     SecStr          630..640
                     /sec_str_type="helix"
                     /note="helix 45"
     SecStr          652..656
                     /sec_str_type="sheet"
                     /note="strand 61"
     SecStr          664..673
                     /sec_str_type="helix"
                     /note="helix 46"
     SecStr          675..679
                     /sec_str_type="sheet"
                     /note="strand 62"
     SecStr          682..695
                     /sec_str_type="helix"
                     /note="helix 47"
     SecStr          698..704
                     /sec_str_type="sheet"
                     /note="strand 63"
     SecStr          708..715
                     /sec_str_type="helix"
                     /note="helix 48"
     SecStr          716..722
                     /sec_str_type="sheet"
                     /note="strand 64"
     SecStr          725..732
                     /sec_str_type="helix"
                     /note="helix 49"
     SecStr          733..737
                     /sec_str_type="sheet"
                     /note="strand 65"
     SecStr          743..766
                     /sec_str_type="helix"
                     /note="helix 50"
     Region          768..995
                     /region_name="Domain 10"
                     /note="NCBI Domains"
     SecStr          769..779
                     /sec_str_type="helix"
                     /note="helix 51"
     Region          785..988
                     /region_name="Cation_ATPase_C"
                     /note="Cation transporting ATPase, C-terminus; pfam00689"
                     /db_xref="CDD:250058"
     SecStr          790..798
                     /sec_str_type="helix"
                     /note="helix 52"
     SecStr          833..858
                     /sec_str_type="helix"
                     /note="helix 53"
     SecStr          867..874
                     /sec_str_type="helix"
                     /note="helix 54"
     Bond            877..889
                     /bond_type="disulfide"
     SecStr          896..915
                     /sec_str_type="helix"
                     /note="helix 55"
     SecStr          932..950
                     /sec_str_type="helix"
                     /note="helix 56"
     SecStr          965..975
                     /sec_str_type="helix"
                     /note="helix 57"
     SecStr          978..991
                     /sec_str_type="helix"
                     /note="helix 58"
ORIGIN      
        1 xmeaahskst eeclayfgvs ettgltpdqv krhlekyghn elpaeegksl welvieqfed
       61 llvrilllaa cisfvlawfe egeetitafv epfvillili anaivgvwqe rnaenaieal
      121 keyepemgkv yradrksvqr ikardivpgd ivevavgdkv padirilsik sttlrvdqsi
      181 ltgesvsvik htepvpdpra vnqdkknmlf sgtniaagka lgivattgvs teigkirdqm
      241 aateqdktpl qqkldefgeq lskvislicv avwlinighf ndpvhggswi rgaiyyfkia
      301 valavaaipe glpavittcl algtrrmakk naivrslpsv etlgctsvic sdktgtlttn
      361 qmsvckmfii dkvdgdfcsl nefsitgsty apegevlknd kpirsgqfdg lvelatical
      421 cndssldfne tkgvyekvge atetalttlv ekmnvfntev rnlskveran acnsvirqlm
      481 kkeftlefsr drksmsvycs pakssraavg nkmfvkgape gvidrcnyvr vgttrvpmtg
      541 pvkekilsvi kewgtgrdtl rclalatrdt ppkreemvld dssrfmeyet dltfvgvvgm
      601 ldpprkevmg siqlcrdagi rvimitgdnk gtaiaicrri gifgeneeva draytgrefd
      661 dlplaeqrea crraccfarv epshkskive ylqsydeita mtgdgvndap alkkaeigia
      721 mgsgtavakt asemvladdn fstivaavee graiynnmkq firylissnv gevvciflta
      781 alglpealip vqllwvnlvt dglpatalgf nppdldimdr pprspkepli sgwlffryma
      841 iggyvgaatv gaaawwfmya edgpgvtyhq lthfmqcted hphfegldce ifeapepmtm
      901 alsvlvtiem cnalnslsen qslmrmppwv niwllgsicl smslhflily vdplpmifkl
      961 kaldltqwlm vlkislpvig ldeilkfiar nyleg
//

LOCUS       2ZBE_A                   995 aa            linear   MAM 27-DEC-2012
DEFINITION  Chain A, Calcium Pump Crystal Structure With Bound Bef3 In The
            Absence Of Calcium And Tg.
ACCESSION   2ZBE_A
VERSION     2ZBE_A  GI:433552066
DBSOURCE    pdb: molecule 2ZBE, chain 65, release Dec 7, 2012;
            deposition: Oct 20, 2007;
            class: Hydrolase;
            source: Mmdb_id: 106112, Pdb_id 1: 2ZBE;
            Exp. method: X-Ray Diffraction.
KEYWORDS    .
SOURCE      Oryctolagus cuniculus (rabbit)
  ORGANISM  Oryctolagus cuniculus
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Mammalia; Eutheria; Euarchontoglires; Glires; Lagomorpha;
            Leporidae; Oryctolagus.
REFERENCE   1  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nakasako,M., Nomura,H. and Ogawa,H.
  TITLE     Crystal structure of the calcium pump of sarcoplasmic reticulum at
            2.6 A resolution
  JOURNAL   Nature 405 (6787), 647-655 (2000)
   PUBMED   10864315
REFERENCE   2  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Nomura,H.
  TITLE     Structural changes in the calcium pump accompanying the
            dissociation of calcium
  JOURNAL   Nature 418 (6898), 605-611 (2002)
   PUBMED   12167852
REFERENCE   3  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Mizutani,T.
  TITLE     Crystal structure of the calcium pump with a bound ATP analogue
  JOURNAL   Nature 430 (6999), 529-535 (2004)
   PUBMED   15229613
REFERENCE   4  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nomura,H. and Tsuda,T.
  TITLE     Lumenal gating mechanism revealed in calcium pump crystal
            structures with phosphate analogues
  JOURNAL   Nature 432 (7015), 361-368 (2004)
   PUBMED   15448704
REFERENCE   5  (residues 1 to 995)
  AUTHORS   Obara,K., Miyashita,N., Xu,C., Toyoshima,I., Sugita,Y., Inesi,G.
            and Toyoshima,C.
  TITLE     Structural role of countertransport revealed in Ca(2+) pump crystal
            structure in the absence of Ca(2+)
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 102 (41), 14489-14496 (2005)
   PUBMED   16150713
REFERENCE   6  (residues 1 to 995)
  AUTHORS   Takahashi,M., Kondou,Y. and Toyoshima,C.
  TITLE     Interdomain communication in calcium pump as revealed in the
            crystal structures with transmembrane inhibitors
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 104 (14), 5800-5805 (2007)
   PUBMED   17389383
REFERENCE   7  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Norimatsu,Y., Iwasawa,S., Tsuda,T. and Ogawa,H.
  TITLE     How processing of aspartylphosphate is coupled to lumenal gating of
            the ion pathway in the calcium pump
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 104 (50), 19831-19836 (2007)
   PUBMED   18077416
REFERENCE   8  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Ogawa,H. and Norimatsu,Y.
  TITLE     Direct Submission
  JOURNAL   Submitted (20-OCT-2007)
COMMENT     On Dec 27, 2012 this sequence version replaced gi:162330210.
            1 Sarcoplasmic/endoplasmic Reticulum Calcium Atpase 1.
FEATURES             Location/Qualifiers
     source          1..995
                     /organism="Oryctolagus cuniculus"
                     /db_xref="taxon:9986"
     Region          5..73
                     /region_name="Cation_ATPase_N"
                     /note="Cation transporter/ATPase, N-terminus; pfam00690"
                     /db_xref="CDD:250059"
     SecStr          10..17
                     /sec_str_type="helix"
                     /note="helix 1"
     SecStr          27..36
                     /sec_str_type="helix"
                     /note="helix 2"
     Region          join(44..122,246..286)
                     /region_name="Domain 1"
                     /note="NCBI Domains"
     SecStr          50..58
                     /sec_str_type="helix"
                     /note="helix 3"
     Region          54..990
                     /region_name="ATPase-IIA1_Ca"
                     /note="sarco/endoplasmic reticulum calcium-translocating
                     P-type ATPase; TIGR01116"
                     /db_xref="CDD:273452"
     SecStr          61..79
                     /sec_str_type="helix"
                     /note="helix 4"
     SecStr          91..117
                     /sec_str_type="helix"
                     /note="helix 5"
     Region          94..342
                     /region_name="E1-E2_ATPase"
                     /note="E1-E2 ATPase; pfam00122"
                     /db_xref="CDD:249606"
     Region          123..245
                     /region_name="Domain 2"
                     /note="NCBI Domains"
     SecStr          127..131
                     /sec_str_type="sheet"
                     /note="strand 1"
     SecStr          138..142
                     /sec_str_type="sheet"
                     /note="strand 2"
     SecStr          150..156
                     /sec_str_type="sheet"
                     /note="strand 3"
     SecStr          162..166
                     /sec_str_type="sheet"
                     /note="strand 4"
     SecStr          167..171
                     /sec_str_type="sheet"
                     /note="strand 5"
     SecStr          174..178
                     /sec_str_type="sheet"
                     /note="strand 6"
     SecStr          187..190
                     /sec_str_type="sheet"
                     /note="strand 7"
     SecStr          207..211
                     /sec_str_type="sheet"
                     /note="strand 8"
     SecStr          213..217
                     /sec_str_type="sheet"
                     /note="strand 9"
     SecStr          218..225
                     /sec_str_type="sheet"
                     /note="strand 10"
     SecStr          232..242
                     /sec_str_type="helix"
                     /note="helix 6"
     SecStr          249..272
                     /sec_str_type="helix"
                     /note="helix 7"
     Region          join(287..309,768..995)
                     /region_name="Domain 3"
                     /note="NCBI Domains"
     SecStr          300..307
                     /sec_str_type="helix"
                     /note="helix 8"
     Region          join(310..357,606..767)
                     /region_name="Domain 4"
                     /note="NCBI Domains"
     SecStr          312..330
                     /sec_str_type="helix"
                     /note="helix 9"
     SecStr          331..335
                     /sec_str_type="sheet"
                     /note="strand 11"
     SecStr          347..353
                     /sec_str_type="sheet"
                     /note="strand 12"
     Het             join(352,354,704)
                     /heterogen="( MG,1000 )"
     Het             352
                     /heterogen="(BEF,1002 )"
     Region          358..605
                     /region_name="Domain 5"
                     /note="NCBI Domains"
     SecStr          362..370
                     /sec_str_type="sheet"
                     /note="strand 13"
     SecStr          371..375
                     /sec_str_type="sheet"
                     /note="strand 14"
     SecStr          376..387
                     /sec_str_type="sheet"
                     /note="strand 15"
     SecStr          394..398
                     /sec_str_type="sheet"
                     /note="strand 16"
     SecStr          409..421
                     /sec_str_type="helix"
                     /note="helix 10"
     Region          419..529
                     /region_name="Hydrolase_like2"
                     /note="Putative hydrolase of sodium-potassium ATPase alpha
                     subunit; pfam13246"
                     /db_xref="CDD:257602"
     SecStr          424..430
                     /sec_str_type="sheet"
                     /note="strand 17"
     SecStr          433..439
                     /sec_str_type="sheet"
                     /note="strand 18"
     SecStr          441..453
                     /sec_str_type="helix"
                     /note="helix 11"
     SecStr          470..478
                     /sec_str_type="helix"
                     /note="helix 12"
     SecStr          480..490
                     /sec_str_type="sheet"
                     /note="strand 19"
     SecStr          492..501
                     /sec_str_type="sheet"
                     /note="strand 20"
     SecStr          510..518
                     /sec_str_type="sheet"
                     /note="strand 21"
     SecStr          519..526
                     /sec_str_type="helix"
                     /note="helix 13"
     SecStr          527..532
                     /sec_str_type="sheet"
                     /note="strand 22"
     SecStr          533..538
                     /sec_str_type="sheet"
                     /note="strand 23"
     SecStr          540..554
                     /sec_str_type="helix"
                     /note="helix 14"
     SecStr          561..569
                     /sec_str_type="sheet"
                     /note="strand 24"
     SecStr          592..597
                     /sec_str_type="sheet"
                     /note="strand 25"
     SecStr          598..602
                     /sec_str_type="sheet"
                     /note="strand 26"
     SecStr          608..618
                     /sec_str_type="helix"
                     /note="helix 15"
     Region          <612..721
                     /region_name="HAD_like"
                     /note="Haloacid dehalogenase-like hydrolases. The haloacid
                     dehalogenase-like (HAD) superfamily includes L-2-haloacid
                     dehalogenase, epoxide hydrolase, phosphoserine
                     phosphatase, phosphomannomutase, phosphoglycolate
                     phosphatase, P-type ATPase, and many others; cd01427"
                     /db_xref="CDD:119389"
     SecStr          620..627
                     /sec_str_type="sheet"
                     /note="strand 27"
     Site            626
                     /site_type="other"
                     /note="motif II"
                     /db_xref="CDD:119389"
     SecStr          630..640
                     /sec_str_type="helix"
                     /note="helix 16"
     SecStr          652..656
                     /sec_str_type="sheet"
                     /note="strand 28"
     SecStr          664..673
                     /sec_str_type="helix"
                     /note="helix 17"
     SecStr          675..679
                     /sec_str_type="sheet"
                     /note="strand 29"
     SecStr          682..695
                     /sec_str_type="helix"
                     /note="helix 18"
     SecStr          698..704
                     /sec_str_type="sheet"
                     /note="strand 30"
     SecStr          708..715
                     /sec_str_type="helix"
                     /note="helix 19"
     SecStr          716..722
                     /sec_str_type="sheet"
                     /note="strand 31"
     SecStr          725..732
                     /sec_str_type="helix"
                     /note="helix 20"
     SecStr          733..737
                     /sec_str_type="sheet"
                     /note="strand 32"
     SecStr          743..766
                     /sec_str_type="helix"
                     /note="helix 21"
     SecStr          769..779
                     /sec_str_type="helix"
                     /note="helix 22"
     Region          785..988
                     /region_name="Cation_ATPase_C"
                     /note="Cation transporting ATPase, C-terminus; pfam00689"
                     /db_xref="CDD:250058"
     SecStr          790..798
                     /sec_str_type="helix"
                     /note="helix 23"
     SecStr          833..858
                     /sec_str_type="helix"
                     /note="helix 24"
     SecStr          867..874
                     /sec_str_type="helix"
                     /note="helix 25"
     Bond            877..889
                     /bond_type="disulfide"
     SecStr          896..915
                     /sec_str_type="helix"
                     /note="helix 26"
     SecStr          932..950
                     /sec_str_type="helix"
                     /note="helix 27"
     SecStr          965..975
                     /sec_str_type="helix"
                     /note="helix 28"
     SecStr          978..991
                     /sec_str_type="helix"
                     /note="helix 29"
ORIGIN      
        1 xmeaahskst eeclayfgvs ettgltpdqv krhlekyghn elpaeegksl welvieqfed
       61 llvrilllaa cisfvlawfe egeetitafv epfvillili anaivgvwqe rnaenaieal
      121 keyepemgkv yradrksvqr ikardivpgd ivevavgdkv padirilsik sttlrvdqsi
      181 ltgesvsvik htepvpdpra vnqdkknmlf sgtniaagka lgivattgvs teigkirdqm
      241 aateqdktpl qqkldefgeq lskvislicv avwlinighf ndpvhggswi rgaiyyfkia
      301 valavaaipe glpavittcl algtrrmakk naivrslpsv etlgctsvic sdktgtlttn
      361 qmsvckmfii dkvdgdfcsl nefsitgsty apegevlknd kpirsgqfdg lvelatical
      421 cndssldfne tkgvyekvge atetalttlv ekmnvfntev rnlskveran acnsvirqlm
      481 kkeftlefsr drksmsvycs pakssraavg nkmfvkgape gvidrcnyvr vgttrvpmtg
      541 pvkekilsvi kewgtgrdtl rclalatrdt ppkreemvld dssrfmeyet dltfvgvvgm
      601 ldpprkevmg siqlcrdagi rvimitgdnk gtaiaicrri gifgeneeva draytgrefd
      661 dlplaeqrea crraccfarv epshkskive ylqsydeita mtgdgvndap alkkaeigia
      721 mgsgtavakt asemvladdn fstivaavee graiynnmkq firylissnv gevvciflta
      781 alglpealip vqllwvnlvt dglpatalgf nppdldimdr pprspkepli sgwlffryma
      841 iggyvgaatv gaaawwfmya edgpgvtyhq lthfmqcted hphfegldce ifeapepmtm
      901 alsvlvtiem cnalnslsen qslmrmppwv niwllgsicl smslhflily vdplpmifkl
      961 kaldltqwlm vlkislpvig ldeilkfiar nyleg
//

LOCUS       2ZBG_A                   995 aa            linear   MAM 18-OCT-2012
DEFINITION  Chain A, Calcium Pump Crystal Structure With Bound Alf4 And Tg In
            The Absence Of Calcium.
ACCESSION   2ZBG_A
VERSION     2ZBG_A  GI:319443869
DBSOURCE    pdb: molecule 2ZBG, chain 65, release Oct 13, 2012;
            deposition: Oct 20, 2007;
            class: Hydrolase;
            source: Mmdb_id: 88154, Pdb_id 1: 2ZBG;
            Exp. method: X-Ray Diffraction.
KEYWORDS    .
SOURCE      Oryctolagus cuniculus (rabbit)
  ORGANISM  Oryctolagus cuniculus
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Mammalia; Eutheria; Euarchontoglires; Glires; Lagomorpha;
            Leporidae; Oryctolagus.
REFERENCE   1  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nakasako,M., Nomura,H. and Ogawa,H.
  TITLE     Crystal structure of the calcium pump of sarcoplasmic reticulum at
            2.6 A resolution
  JOURNAL   Nature 405 (6787), 647-655 (2000)
   PUBMED   10864315
REFERENCE   2  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Nomura,H.
  TITLE     Structural changes in the calcium pump accompanying the
            dissociation of calcium
  JOURNAL   Nature 418 (6898), 605-611 (2002)
   PUBMED   12167852
REFERENCE   3  (residues 1 to 995)
  AUTHORS   Toyoshima,C. and Mizutani,T.
  TITLE     Crystal structure of the calcium pump with a bound ATP analogue
  JOURNAL   Nature 430 (6999), 529-535 (2004)
   PUBMED   15229613
REFERENCE   4  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Nomura,H. and Tsuda,T.
  TITLE     Lumenal gating mechanism revealed in calcium pump crystal
            structures with phosphate analogues
  JOURNAL   Nature 432 (7015), 361-368 (2004)
   PUBMED   15448704
REFERENCE   5  (residues 1 to 995)
  AUTHORS   Obara,K., Miyashita,N., Xu,C., Toyoshima,I., Sugita,Y., Inesi,G.
            and Toyoshima,C.
  TITLE     Structural role of countertransport revealed in Ca(2+) pump crystal
            structure in the absence of Ca(2+)
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 102 (41), 14489-14496 (2005)
   PUBMED   16150713
REFERENCE   6  (residues 1 to 995)
  AUTHORS   Takahashi,M., Kondou,Y. and Toyoshima,C.
  TITLE     Interdomain communication in calcium pump as revealed in the
            crystal structures with transmembrane inhibitors
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 104 (14), 5800-5805 (2007)
   PUBMED   17389383
REFERENCE   7  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Norimatsu,Y., Iwasawa,S., Tsuda,T. and Ogawa,H.
  TITLE     How processing of aspartylphosphate is coupled to lumenal gating of
            the ion pathway in the calcium pump
  JOURNAL   Proc. Natl. Acad. Sci. U.S.A. 104 (50), 19831-19836 (2007)
   PUBMED   18077416
REFERENCE   8  (residues 1 to 995)
  AUTHORS   Toyoshima,C., Ogawa,H. and Norimatsu,Y.
  TITLE     Direct Submission
  JOURNAL   Submitted (20-OCT-2007)
COMMENT     On Jan 19, 2011 this sequence version replaced gi:162330213.
            1 Sarcoplasmic/endoplasmic Reticulum Calcium Atpase 1.
FEATURES             Location/Qualifiers
     source          1..995
                     /organism="Oryctolagus cuniculus"
                     /db_xref="taxon:9986"
     Region          5..73
                     /region_name="Cation_ATPase_N"
                     /note="Cation transporter/ATPase, N-terminus; pfam00690"
                     /db_xref="CDD:250059"
     SecStr          10..17
                     /sec_str_type="helix"
                     /note="helix 1"
     SecStr          27..37
                     /sec_str_type="helix"
                     /note="helix 2"
     Region          join(44..118,246..309)
                     /region_name="Domain 1"
                     /note="NCBI Domains"
     SecStr          50..58
                     /sec_str_type="helix"
                     /note="helix 3"
     Region          54..990
                     /region_name="ATPase-IIA1_Ca"
                     /note="sarco/endoplasmic reticulum calcium-translocating
                     P-type ATPase; TIGR01116"
                     /db_xref="CDD:273452"
     SecStr          61..76
                     /sec_str_type="helix"
                     /note="helix 4"
     SecStr          90..109
                     /sec_str_type="helix"
                     /note="helix 5"
     Region          94..342
                     /region_name="E1-E2_ATPase"
                     /note="E1-E2 ATPase; pfam00122"
                     /db_xref="CDD:249606"
     Region          119..245
                     /region_name="Domain 2"
                     /note="NCBI Domains"
     SecStr          127..131
                     /sec_str_type="sheet"
                     /note="strand 1"
     SecStr          138..142
                     /sec_str_type="sheet"
                     /note="strand 2"
     SecStr          150..156
                     /sec_str_type="sheet"
                     /note="strand 3"
     SecStr          162..166
                     /sec_str_type="sheet"
                     /note="strand 4"
     SecStr          167..171
                     /sec_str_type="sheet"
                     /note="strand 5"
     SecStr          174..178
                     /sec_str_type="sheet"
                     /note="strand 6"
     SecStr          186..189
                     /sec_str_type="sheet"
                     /note="strand 7"
     SecStr          207..211
                     /sec_str_type="sheet"
                     /note="strand 8"
     SecStr          213..217
                     /sec_str_type="sheet"
                     /note="strand 9"
     SecStr          218..225
                     /sec_str_type="sheet"
                     /note="strand 10"
     SecStr          232..241
                     /sec_str_type="helix"
                     /note="helix 6"
     SecStr          249..274
                     /sec_str_type="helix"
                     /note="helix 7"
     SecStr          294..307
                     /sec_str_type="helix"
                     /note="helix 8"
     Region          join(310..346,741..995)
                     /region_name="Domain 3"
                     /note="NCBI Domains"
     SecStr          312..330
                     /sec_str_type="helix"
                     /note="helix 9"
     SecStr          331..335
                     /sec_str_type="sheet"
                     /note="strand 11"
     SecStr          339..346
                     /sec_str_type="helix"
                     /note="helix 10"
     Region          join(347..355,607..740)
                     /region_name="Domain 4"
                     /note="NCBI Domains"
     SecStr          347..353
                     /sec_str_type="sheet"
                     /note="strand 12"
     Het             join(352,354,704)
                     /heterogen="( MG,1000 )"
     Het             352
                     /heterogen="(ALF,1001 )"
     Region          356..606
                     /region_name="Domain 5"
                     /note="NCBI Domains"
     SecStr          357..360
                     /sec_str_type="sheet"
                     /note="strand 13"
     SecStr          362..370
                     /sec_str_type="sheet"
                     /note="strand 14"
     SecStr          371..375
                     /sec_str_type="sheet"
                     /note="strand 15"
     SecStr          376..387
                     /sec_str_type="sheet"
                     /note="strand 16"
     SecStr          394..398
                     /sec_str_type="sheet"
                     /note="strand 17"
     SecStr          409..421
                     /sec_str_type="helix"
                     /note="helix 11"
     Region          419..529
                     /region_name="Hydrolase_like2"
                     /note="Putative hydrolase of sodium-potassium ATPase alpha
                     subunit; pfam13246"
                     /db_xref="CDD:257602"
     SecStr          424..430
                     /sec_str_type="sheet"
                     /note="strand 18"
     SecStr          433..439
                     /sec_str_type="sheet"
                     /note="strand 19"
     SecStr          441..453
                     /sec_str_type="helix"
                     /note="helix 12"
     SecStr          471..478
                     /sec_str_type="helix"
                     /note="helix 13"
     SecStr          479..487
                     /sec_str_type="sheet"
                     /note="strand 20"
     SecStr          493..502
                     /sec_str_type="sheet"
                     /note="strand 21"
     SecStr          511..518
                     /sec_str_type="sheet"
                     /note="strand 22"
     SecStr          519..526
                     /sec_str_type="helix"
                     /note="helix 14"
     SecStr          527..532
                     /sec_str_type="sheet"
                     /note="strand 23"
     SecStr          533..538
                     /sec_str_type="sheet"
                     /note="strand 24"
     SecStr          540..554
                     /sec_str_type="helix"
                     /note="helix 15"
     SecStr          560..569
                     /sec_str_type="sheet"
                     /note="strand 25"
     SecStr          592..597
                     /sec_str_type="sheet"
                     /note="strand 26"
     SecStr          598..605
                     /sec_str_type="sheet"
                     /note="strand 27"
     SecStr          608..618
                     /sec_str_type="helix"
                     /note="helix 16"
     Region          <612..721
                     /region_name="HAD_like"
                     /note="Haloacid dehalogenase-like hydrolases. The haloacid
                     dehalogenase-like (HAD) superfamily includes L-2-haloacid
                     dehalogenase, epoxide hydrolase, phosphoserine
                     phosphatase, phosphomannomutase, phosphoglycolate
                     phosphatase, P-type ATPase, and many others; cd01427"
                     /db_xref="CDD:119389"
     SecStr          620..627
                     /sec_str_type="sheet"
                     /note="strand 28"
     Site            626
                     /site_type="other"
                     /note="motif II"
                     /db_xref="CDD:119389"
     SecStr          631..640
                     /sec_str_type="helix"
                     /note="helix 17"
     SecStr          652..656
                     /sec_str_type="sheet"
                     /note="strand 29"
     SecStr          664..673
                     /sec_str_type="helix"
                     /note="helix 18"
     SecStr          675..679
                     /sec_str_type="sheet"
                     /note="strand 30"
     SecStr          682..695
                     /sec_str_type="helix"
                     /note="helix 19"
     SecStr          698..704
                     /sec_str_type="sheet"
                     /note="strand 31"
     SecStr          708..715
                     /sec_str_type="helix"
                     /note="helix 20"
     SecStr          716..722
                     /sec_str_type="sheet"
                     /note="strand 32"
     SecStr          725..732
                     /sec_str_type="helix"
                     /note="helix 21"
     SecStr          733..737
                     /sec_str_type="sheet"
                     /note="strand 33"
     SecStr          743..782
                     /sec_str_type="helix"
                     /note="helix 22"
     Region          785..988
                     /region_name="Cation_ATPase_C"
                     /note="Cation transporting ATPase, C-terminus; pfam00689"
                     /db_xref="CDD:250058"
     SecStr          792..799
                     /sec_str_type="helix"
                     /note="helix 23"
     SecStr          801..809
                     /sec_str_type="helix"
                     /note="helix 24"
     SecStr          833..856
                     /sec_str_type="helix"
                     /note="helix 25"
     SecStr          867..874
                     /sec_str_type="helix"
                     /note="helix 26"
     Bond            877..889
                     /bond_type="disulfide"
     SecStr          897..915
                     /sec_str_type="helix"
                     /note="helix 27"
     SecStr          932..949
                     /sec_str_type="helix"
                     /note="helix 28"
     SecStr          968..975
                     /sec_str_type="helix"
                     /note="helix 29"
     SecStr          979..991
                     /sec_str_type="helix"
                     /note="helix 30"
ORIGIN      
        1 xmeaahskst eeclayfgvs ettgltpdqv krhlekyghn elpaeegksl welvieqfed
       61 llvrilllaa cisfvlawfe egeetitafv epfvillili anaivgvwqe rnaenaieal
      121 keyepemgkv yradrksvqr ikardivpgd ivevavgdkv padirilsik sttlrvdqsi
      181 ltgesvsvik htepvpdpra vnqdkknmlf sgtniaagka lgivattgvs teigkirdqm
      241 aateqdktpl qqkldefgeq lskvislicv avwlinighf ndpvhggswi rgaiyyfkia
      301 valavaaipe glpavittcl algtrrmakk naivrslpsv etlgctsvic sdktgtlttn
      361 qmsvckmfii dkvdgdfcsl nefsitgsty apegevlknd kpirsgqfdg lvelatical
      421 cndssldfne tkgvyekvge atetalttlv ekmnvfntev rnlskveran acnsvirqlm
      481 kkeftlefsr drksmsvycs pakssraavg nkmfvkgape gvidrcnyvr vgttrvpmtg
      541 pvkekilsvi kewgtgrdtl rclalatrdt ppkreemvld dssrfmeyet dltfvgvvgm
      601 ldpprkevmg siqlcrdagi rvimitgdnk gtaiaicrri gifgeneeva draytgrefd
      661 dlplaeqrea crraccfarv epshkskive ylqsydeita mtgdgvndap alkkaeigia
      721 mgsgtavakt asemvladdn fstivaavee graiynnmkq firylissnv gevvciflta
      781 alglpealip vqllwvnlvt dglpatalgf nppdldimdr pprspkepli sgwlffryma
      841 iggyvgaatv gaaawwfmya edgpgvtyhq lthfmqcted hphfegldce ifeapepmtm
      901 alsvlvtiem cnalnslsen qslmrmppwv niwllgsicl smslhflily vdplpmifkl
      961 kaldltqwlm vlkislpvig ldeilkfiar nyleg
//

